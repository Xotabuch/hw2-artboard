const burger = document.querySelector(".burger");
const nav = document.querySelector(".nav-list");
console.log(nav);

burger.addEventListener("click", function (e) {
  e.preventDefault();
  burger.classList.toggle("active");
  nav.classList.toggle("active");
});
